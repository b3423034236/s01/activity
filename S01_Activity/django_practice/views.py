from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict
# from django.template import loader

from .models import GroceryItem

def index(request):
    grocery_item_list = GroceryItem.objects.all()
    context = {
    'grocery_item_list': grocery_item_list,
    'user': request.user
    }
    return render(request, "django_practice/index.html", context)


def grocery_item(request, grocery_item_id):
    # response = "You are viewing the details of %s"
    # return HttpResponse(response)

    grocery_item = model_to_dict(GroceryItem.objects.get(pk=grocery_item_id))
    return render(request, "django_practice/grocery_item.html", grocery_item)


def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    for indiv_user in users:
        if indiv_user.username == "johndoe":
            is_user_registered = True
            break

    if is_user_registered == False:
        user = User()
        user.username = 'johndoe'
        user.first_name = "John"
        user.last_name = "Doe"
        user.email = "johndoe@mail.com"
        user.set_password("john1234")
        user.is_staff = False
        user.is_Active = True
        user.save()
        context = {
            "first_name": user.first_name,
            "last_name": user.last_name,
        }
    return render(request, "django_practice/register.html", context)


def change_password(request):
    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")
    print(user)

    if user is not None:
        authenticated_user = User.objects.get(username="johndoe")
        authenticated_user.set_password("john1")
        authenticated_user.save()
        is_user_authenticated = True

    context = {
    "is_user_authenticated": is_user_authenticated
    }
    return render(request, "django_practice/change_password.html", context)


def login_view(request):
    username = "johndoe"
    password = "john1"
    user = authenticate(username=username, password=password)
    context = {
    "is_user_authenticated": False
    }
    print(user)
    if user is not None:
        login(request, user)
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("index")